﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Doors.Data;
using Doors.Models;

namespace Doors.Controllers
{
    public class DoorsController : Controller
    {
        private readonly AppDBContext _context;

        public DoorsController(AppDBContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var appDBContext = _context.Doors.Include(d => d.Order);
            return View(await appDBContext.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Doors == null)
            {
                return NotFound();
            }

            var door = await _context.Doors
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (door == null)
            {
                return NotFound();
            }

            return View(door);
        }

        public IActionResult Create()
        {
            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Material,Price,AdditionalProperties,OrderId")] Door door)
        {
            _context.Add(door);
            await _context.SaveChangesAsync();

            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", door.OrderId);
            return View(door);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Doors == null)
            {
                return NotFound();
            }

            var door = await _context.Doors.FindAsync(id);
            if (door == null)
            {
                return NotFound();
            }

            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", door.OrderId);
            return View(door);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Material,Price,AdditionalProperties,OrderId")] Door door)
        {
            if (id != door.Id)
            {
                return NotFound();
            }

            try
            {
                _context.Update(door);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoorExists(door.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            ViewData["OrderId"] = new SelectList(_context.Orders, "Id", "Id", door.OrderId);
            return View(door);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Doors == null)
            {
                return NotFound();
            }

            var door = await _context.Doors
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (door == null)
            {
                return NotFound();
            }

            return View(door);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Doors == null)
            {
                return Problem("Entity set 'AppDBContext.Doors'  is null.");
            }
            var door = await _context.Doors.FindAsync(id);
            if (door != null)
            {
                _context.Doors.Remove(door);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DoorExists(int id)
        {
            return (_context.Doors?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
