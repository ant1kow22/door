﻿namespace Doors.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string CustomerContactInfo { get; set; }
        public string AdditionalProperties { get; set; }

        public List<Door> Doors { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}
