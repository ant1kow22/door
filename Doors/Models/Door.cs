﻿namespace Doors.Models
{
    public class Door
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Material { get; set; }
        public decimal Price { get; set; }
        public string AdditionalProperties { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
